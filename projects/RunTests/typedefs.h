#pragma once
#include <iostream>


#define TPS_VERIFY(a) {\
    if(a) std::cout << ". ";\
    else  std::cout << std::endl << "    FAIL  "  << __LINE__ << " : " << __FUNCTION__ << "\t" << #a << std::endl;\
}

#define TPS_VERIFY_MSG(a,msg) {\
    if(a) std::cout << ". ";\
    else  std::cout << std::endl << "\t" << __LINE__ << " : " << __FUNCTION__ << "\t" << #a << "\t - FAIL with a message (" << #msg << ")" << std::endl;\
}

#define TPS_ASSERT(a) {\
    if(a) std::cout << ". ";\
    else  {\
        std::cout << std::endl << "    FAIL with a CRITICAL ERROR   "  << __LINE__ << " : " << __FUNCTION__ << "\t\t" << #a << std::endl;\
        return;\
    }\
}

#define TPS_ASSERT_MSG(a,msg) {\
    if(a) std::cout << ". ";\
    else  {\
        std::cout << std::endl << "\t" << __LINE__ << " : " << __FUNCTION__ << "\t" << #a << "\t - FAIL with a CRITICAL ERROR! and a message (" << #msg << ")" << std::endl;\
        return;\
    }\
}

#define  TEST(cls){\
    {\
        std::cout << #cls << "\t";\
        cls()();\
        std::cout << std::endl;\
    }\
}

//class RepetitionsTest {
//public:
//    typedef RepetitionsTest class_name;
//    typedef Repetitions     tested;

//private:
//    static void testEmpty() {
//        tested cls;
//    }

//public:
//    void operator()(){
//        testEmpty();
//    }
//};
