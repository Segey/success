#-------------------------------------------------
#
# Project created by QtCreator 2015-03-30T14:33:17
#
#-------------------------------------------------

QT       += testlib
QT       -= gui

TARGET = RunTests
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++1y
INCLUDEPATH += $$PWD/../../../ps2/

SOURCES += \
    main.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"

HEADERS += \
    typedefs.h \
    ../../../ps2/libs/cpp/qt/rand.h \
    ../../Success/classes/number_generator.h \
    ../../Success/classes/number_generator_test.h \
    ../../Success/classes/values.h \
    ../../Success/classes/values_test.h \
    ../Success/classes/sign_generator.h \
    ../Success/classes/sign_generator_test.h \
    ../Success/classes/max_expression_test.h \
    ../Success/classes/math_expression.h
