#include <QString>
#include <QtTest>
#include "typedefs.h"
#include "../../Success/classes/max_expression_test.h"
#include "../../Success/classes/number_generator_test.h"
#include "../../Success/classes/sign_generator_test.h"

int main(int, char *[])
{
    srand (time(nullptr));

    TEST(NumberGeneratorTest);
    TEST(SignGeneratorTest);
    TEST(MaxExpressionTest);

    return EXIT_SUCCESS;
}
