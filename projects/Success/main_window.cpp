#include "main_window.h"
#include <QHBoxLayout>
#include <QDebug>
#include <QIntValidator>
#include <QMessageBox>
#include <QFile>
#include "classes/sign_generator.h"
#include "classes/number_generator.h"
#include "classes/math_expression.h"

MainWindow::MainWindow(QWidget* parent) :
    inherited(parent)
    , m_textLabel(nullptr)
    , m_questionLabel(nullptr)
    , m_resultLabel(nullptr)
    , m_btnResult(nullptr)
    , m_btnBegin(nullptr)
    , m_btnEnd(nullptr)
    , m_btnNext(nullptr)
    , m_edit(nullptr)
{
    init();
    initSignals();
    initBeforeWork();
}
MainWindow::~MainWindow()
{
}
void MainWindow::initSignals()
{
    connect(m_btnResult,QPushButton::clicked, this, onClickResult);
    connect(m_btnBegin, QPushButton::clicked, this, onClickBegin);
    connect(m_btnEnd,   QPushButton::clicked, this, onClickEnd);
    connect(m_btnNext,  QPushButton::clicked, this, onClickNext);
    connect(m_edit,     QLineEdit::returnPressed, this, onClickNext);
}
QLayout* MainWindow::initTopInfoLayout()
{
    m_textLabel= new QLabel(this);
    m_textLabel->setText("Работа с двухначными числами");
    auto font = m_textLabel->font();
    font.setPixelSize(16);
    font.setBold(true);
    m_textLabel->setFont(font);

    auto lay = new QHBoxLayout;
    lay->addWidget(m_textLabel,0,Qt::AlignCenter);
    return lay;
}
QLayout* MainWindow::initCentralLayout()
{
    auto validator = new QIntValidator(this);

    m_questionLabel= new QLabel(this);
    auto font = m_questionLabel->font();
    font.setPixelSize(30);
    m_questionLabel->setFont(font);

    m_edit= new QLineEdit(this);
    m_edit->setFont(font);
    m_edit->setValidator(validator);

    auto center = new QHBoxLayout;
    center->addWidget(m_questionLabel, 3);
    center->addWidget(m_edit);
    return center;
}
QLayout* MainWindow::initResultInfoLayout()
{
    m_resultLabel = new QLabel(this);

    auto lay = new QVBoxLayout;
    lay->addWidget(m_resultLabel);
    return lay;
}
QLayout* MainWindow::initButtonsLayout()
{
    m_btnResult = new QPushButton("Результаты", this);
    m_btnBegin  = new QPushButton("Начать", this);
    m_btnEnd    = new QPushButton("Закончить", this);
    m_btnNext   = new QPushButton("Далее", this);

    auto lay = new QHBoxLayout;
    lay->addWidget(m_btnResult);
    lay->addStretch();
    lay->addWidget(m_btnBegin);
    lay->addWidget(m_btnEnd);
    lay->addWidget(m_btnNext);
    return lay;
}
void MainWindow::init()
{
    setWindowTitle("Success v.1.03 - VLAD вперед!");
    setMinimumSize(420, 280);

    auto line = new QFrame(this);
    line->setFrameShape(QFrame::HLine);
    line->setFrameShadow(QFrame::Sunken);

    auto lay = new QVBoxLayout;
    lay->addLayout(initTopInfoLayout());
    lay->addStretch();
    lay->addLayout(initCentralLayout());
    lay->addStretch();
    lay->addLayout(initResultInfoLayout());
    lay->addWidget(line);
    lay->addLayout(initButtonsLayout());

    auto w = new QWidget(this);
    w->setLayout(lay);
    setCentralWidget(w);
}
QPair<int, int> MainWindow::showResult() const
{
    int flaw = 0;
    int right = 0;

    std::for_each(m_values.cbegin(), m_values.cend(), [&](auto const& v) {
        if(v.answer() != -1) {
            const bool is = v.result() == v.answer();
            is ? ++right : ++flaw;
        }
    });

    return qMakePair(right, flaw);
}
void MainWindow::initBeforeWork()
{
    m_btnResult->setDisabled(m_values.empty());
    m_btnBegin->setDisabled(false);
    m_btnEnd->setDisabled(true);
    m_btnNext->setDisabled(true);
    m_questionLabel->clear();
    m_edit->setVisible(false);
    m_edit->clear();
    m_resultLabel->clear();
}
void MainWindow::initWork()
{
    m_btnResult->setDisabled(m_values.empty());
    m_btnBegin->setDisabled(true);
    m_btnEnd->setDisabled(false);
    m_btnNext->setDisabled(false);
    m_questionLabel->clear();
    m_edit->setVisible(true);
    m_edit->clear();
    m_values.clear();
    m_resultLabel->clear();
}
void MainWindow::initAfterWork()
{
    m_btnResult->setDisabled(m_values.empty());
    m_btnBegin->setDisabled(false);
    m_btnEnd->setDisabled(true);
    m_btnNext->setDisabled(true);
    m_questionLabel->clear();
    m_edit->setVisible(false);
    m_edit->clear();
}
MainWindow::value_t MainWindow::normalizeData(QPair<int, int> const& values, value_t::sign_t sign)
{
    value_t result(values.first, values.second, sign, -1);

    switch(sign) {
        default:
        case SIGN::PLUS:
        case SIGN::MULTIPLICATION:
            break;
        case SIGN::MINUS: {
            if(result.first() < result.second()) {
                result.setFirst(values.second);
                result.setSecond(values.first);
            }
            break;
        }
        case SIGN::DIVISION: {
            result.setFirst(values.second * values.first);
            break;
        }
    }
    return result;
}
void MainWindow::generateNextValues()
{
    auto const& data = normalizeData(NumberGenerator::generate(), SignGenerator::generate());
    m_values.push_back(data);
    showQuestion(data);
}
int MainWindow::answerNumber() const
{
    auto const& s = m_edit->text();

    bool is = true;
    const int answer = s.toInt(&is);
    return is ? answer : -1;
}
QTime MainWindow::elapsedTime() const
{
    QTime time(0,0,0);
    return time.addMSecs(m_time.elapsed());
}
void MainWindow::showQuestion(value_t const& values)
{
    m_questionLabel->setText(QString("%1 %2 %3 = ").arg(values.first()).arg(toString(values.sign())).arg(values.second()));
}
void MainWindow::saveResult() const
{
    QFile file("D:\\results.txt");
    if (!file.open(QIODevice::Append | QIODevice::Text)) return;

    auto const& result = showResult();
    auto const& data = QString("%1 (%2) %3 %4 %5").arg(QDateTime::currentDateTime().toString()).arg(elapsedTime().toString()).arg(result.first).arg(result.second).arg(result.first + result.second);

    QTextStream out(&file);
    out << data << "\n";

    file.close();
}
void MainWindow::onClickResult(){
    QString result = "<table><caption> Таблица результатов </caption><tr><th> Значение 1 </th><th> Значение 2 </th><th> Ответ </th><th> Правильный ответ </th></tr>";
    std::for_each(m_values.cbegin(), m_values.cend(), [&](auto const& v) {
        if(v.answer() == -1) return;
        result += QString("<tr><td>%1</td><td>%2</td><td>%3</td><td>%4</td></tr>").arg(v.first()).arg(v.second()).arg(v.answer()).arg(v.result());
    });
    result +="</table>";

    QMessageBox msgBox;
    msgBox.setText(result);
    msgBox.setInformativeText("  Результаты будут сохранены!");
    msgBox.setStandardButtons(QMessageBox::Ok);
    msgBox.exec();
}
void MainWindow::onClickBegin(){
    initWork();
    generateNextValues();
    m_time.start();
}
void MainWindow::onClickEnd(){
    if(!m_values.empty()) m_values.back().setAnswer(answerNumber());
    initAfterWork();
    saveResult();
}
void MainWindow::onClickNext(){
    if(!m_values.empty()) m_values.back().setAnswer(answerNumber());

    auto pair = showResult();
    generateNextValues();
    m_edit->clear();

    m_resultLabel->setText(QString("Верных ответов: %1\nНеверных ответов: %2\nВсего: %3 ответов").arg(pair.first).arg(pair.second).arg(pair.first + pair.second));
}

