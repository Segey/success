#-------------------------------------------------
#
# Project created by QtCreator 2015-04-02T15:17:50
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Success
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++1y

INCLUDEPATH += $$PWD/../../../ps2/

SOURCES += main.cpp \
    main_window.cpp

HEADERS  += \
    main_window.h \
    classes/number_generator.h \
    classes/values.h \
    classes/sign_generator.h \
    classes/math_expression.h

FORMS    +=
