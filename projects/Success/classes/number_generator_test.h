#pragma once
#include "number_generator.h"

class NumberGeneratorTest {
public:
    typedef NumberGeneratorTest class_name;
    typedef NumberGenerator     tested;

    static bool testValue1(int val){
        return val >= 10 && val <= 99;
    }
    static bool testValue2(int val){
        return val >= 1 && val <= 9;
    }

    static void testCorrectValues(){
        tested cls;
        auto values = cls.generate();
        auto _1 = testValue1(values.first);
        auto _2 = testValue2(values.second);

        TPS_VERIFY(_1 == _2);
    }

public:
    void operator()(){
        testCorrectValues();
    }
};
