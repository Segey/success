#pragma once
#include "sign_generator.h"

class MathExpression {
public:
    typedef MathExpression  class_name;
    typedef SIGN            sign_t;

private:
    int    m_first;
    int    m_second;
    sign_t m_sign;
    int    m_answer;


public:
    MathExpression(int first = 0, int second = 0, sign_t sign = sign_t::INVALID, int answer = 0)
        : m_first(first), m_second(second), m_sign(sign), m_answer(answer) {
    }
    int first() const {
       return m_first;
    }
    int second() const {
        return m_second;
    }
    int answer() const {
        return m_answer;
    }
    int result() const {
        switch(m_sign) {
            case sign_t::MINUS: return m_first - m_second;
            case sign_t::PLUS:  return m_first + m_second;
            case sign_t::MULTIPLICATION: return m_first * m_second;
            default:
            case sign_t::DIVISION: return m_first / m_second;
        }
    }
    void setFirst(int first) {
        m_first = first;
    }
    void setSecond(int second) {
        m_second = second;
    }
    void setAnswer(int answer) {
        m_answer = answer;
    }
    void setSign(sign_t sign) {
        m_sign = sign;
    }
    sign_t sign() const{
        return m_sign;
    }
};

