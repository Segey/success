#pragma once
#include "math_expression.h"

class MaxExpressionTest {
public:
    typedef MaxExpressionTest class_name;
    typedef MathExpression     tested;

    static void testEmpty(){
        tested cls;
        TPS_VERIFY(cls.first() == 0);
        TPS_VERIFY(cls.second() == 0);
    }
    static void testInitFirst(){
        tested cls;
        cls.setFirst(10);
        TPS_VERIFY(cls.first() == 10);
    }
    static void testInitSecond(){
        tested cls;
        cls.setSecond(10);
        TPS_VERIFY(cls.second() == 10);
    }
    static void testInitAssign(){
        tested cls = tested(10,20, SIGN::MULTIPLICATION);
        TPS_VERIFY(cls.first() == 10);
        TPS_VERIFY(cls.second() == 20);
        TPS_VERIFY(cls.result() == 200);
    }
    static void testSignPlus(){
        tested cls = tested(10,20, SIGN::PLUS);
        TPS_VERIFY(cls.first() == 10);
        TPS_VERIFY(cls.second() == 20);
        TPS_VERIFY(cls.result() == 30);
    }
    static void emptyAnswerAfterCreation(){
        tested cls;
        TPS_VERIFY(cls.answer() == 0);
    }
    static void getAnswerAfterInit(){
        tested cls;
        cls.setAnswer(33);
        TPS_VERIFY(cls.answer() == 33);
    }
public:
    void operator()(){
        testEmpty();
        testInitFirst();
        testInitSecond();
        testInitAssign();
        getAnswerAfterInit();
        testSignPlus();
    }
};

