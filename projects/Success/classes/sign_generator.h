#pragma once
#include "ps2/qt/rand.h"

enum class SIGN : std::uint_fast8_t {
    INVALID  = 0
    , MINUS   = 1
    , PLUS = 2
    , MULTIPLICATION = 3
    , DIVISION = 4
};

class SignGenerator {
public:
    typedef SignGenerator  class_name;
    typedef SIGN           type_t;

public:
   static type_t generate() {
        return static_cast<SIGN>(ps2::randInt(1,4));
    }
};

inline QString toString(SIGN sign) {
    switch(sign) {
        case SIGN::MINUS: return QString("-");
        case SIGN::PLUS:  return QString("+");
        case SIGN::MULTIPLICATION: return QString("*");
        default:
        case SIGN::DIVISION: return QString("/");
    }
}
