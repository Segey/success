#pragma once
#include <QPair>
#include "ps2/qt/rand.h"
#include <QObject>

class NumberGenerator {
public:
    typedef NumberGenerator  class_name;
    typedef QPair<int, int>  values_t;

private:
    static int twoNum() {
        return ps2::randInt(10,99);
    }
    static int oneNum() {
        return ps2::randInt(1,9);
    }

public:
   static values_t generate() {
        const bool is = ps2::randBool();
        if(is) return values_t(twoNum() , oneNum());
        return values_t( oneNum(), twoNum());
    }
};
