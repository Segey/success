#pragma once
#include "sign_generator.h"

class SignGeneratorTest {
public:
    typedef SignGeneratorTest class_name;
    typedef SignGenerator     tested;

    static void testCorrectValues(){
        tested cls;
        auto sign = cls.generate();
        TPS_VERIFY(static_cast<SIGN>(1) <= sign && sign <= static_cast<SIGN>(4));
    }

public:
    void operator()(){
        testCorrectValues();
    }
};
