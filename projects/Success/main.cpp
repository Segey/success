#include "main_window.h"
#include <QApplication>
#include "classes/number_generator.h"

int main(int argc, char *argv[])
{
    srand (time(nullptr));

    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
