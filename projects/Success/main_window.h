#pragma once
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QMainWindow>
#include <QList>
#include <QTime>
#include "classes/math_expression.h"

class MainWindow : public QMainWindow {
Q_OBJECT
public:
    typedef MainWindow     class_name;
    typedef QMainWindow    inherited;
    typedef MathExpression value_t;
    typedef QList<value_t> values_t;

private:
    values_t     m_values;
    QLabel*      m_textLabel;
    QLabel*      m_questionLabel;
    QLabel*      m_resultLabel;
    QPushButton* m_btnResult;
    QPushButton* m_btnBegin;
    QPushButton* m_btnEnd;
    QPushButton* m_btnNext;
    QLineEdit*   m_edit;
    QTime        m_time;

    void init();
    void initSignals();
    void initWork();
    void initBeforeWork();
    void initAfterWork();
    QLayout* initTopInfoLayout();
    QLayout* initCentralLayout();
    QLayout* initResultInfoLayout();
    QLayout* initButtonsLayout();
    int answerNumber() const;
    void generateNextValues();
    QTime elapsedTime() const;
    void showQuestion(value_t const& values);
    QPair<int, int> showResult() const;
    void saveResult() const;
    value_t normalizeData(QPair<int, int> const& values, value_t::sign_t sign);

private slots:
    void onClickResult();
    void onClickNext();
    void onClickBegin();
    void onClickEnd();

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
};
